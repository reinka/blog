#!/usr/bin/env bash

### insall docker
sudo apt update
sudo apt install apt-transport-https ca-certificates curl gnupg-agent \
   software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
"deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) stable"

sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io

# post install steps
sudo usermod -aG docker $USER

### install docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.24\
.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

### build service
cd ../docker_ros_tf
sudo chmod +x scripts/talker.py

# comment out GPU relevant parts since this is a CPU only instance
sed -i -e 's/latest-gpu-/latest-/' Dockerfile
sed -i -e 's/runtime:/#runtime:/' docker-compose.yml

# start container in detached mode
sudo docker-compose up -d
