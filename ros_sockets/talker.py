#!/usr/bin/env python3
import socket
import tensorflow as tf
import rospy

from std_msgs.msg import String

def talker():
    pub = rospy.Publisher('chatter', String, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10)  # 10hz

    # NEW: connect to socket and initialize counter vars
    HOST = socket.gethostbyname("localhost")
    PORT = 65432
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    iteration = 0
    iteration_max = 15

    with tf.Session() as sess:
        while not rospy.is_shutdown():
            hello_str = "hello world %s" % sess.run(
                tf.random.normal(shape=(1,)))  # sample from standard normal
            rospy.loginfo(hello_str)
            pub.publish(hello_str)

            # NEW: send kill signal once iteration_max is reached
            iteration += 1
            if iteration == iteration_max:
                with s:
                    s.sendall(b'killall')

            rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
