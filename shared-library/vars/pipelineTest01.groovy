def call(Map params = [:]) {
    pipeline {
        agent { label node_label }

        environment {
            SECRET_CREDENTIALS = credentials('super_secret')
            // Custom environment extension
            
            ANOTHER_SECRET_CREDS = credentials('another_secret')
            TEST = 'Hello world.'

        }

        stages {
            stage('Init') {
                steps {
                    // Do something you do in every pipeline
                    echo 'Init pipeline.'
                    script {
                        // Custom init extension
                        
                        
                    }
                }
            }

            stage('Checkout') {
                steps{
                    checkout scm
                }
            }

            // Custom stages
            
            stage('Test') {
                steps {
                    echo "Hello ${env.TEST}"
                }
            }

        }
    }
}