#!/usr/bin/env python3
import jinja2
import pathlib

class Pipeline:
    def __init__(self, tmpl_path):
        self.tmpl_path = tmpl_path
        self.jinja2_env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(self.tmpl_path))

    def generate(self, pipline_tmpl):
        tmpl = self.jinja2_env.get_template(pipline_tmpl)
        return tmpl.render()


def generate_shared_lib():
    root = pathlib.Path(__file__).parent.parent.parent
    tmpl_path = root / 'resources' / 'template'
    pipeline = Pipeline(tmpl_path)

    vars_path = root / 'vars'
    # if vars folder does not exist create it
    vars_path.mkdir(parents=True, exist_ok=True)

    # travers through templates, generate their groovy files
    # and place them under vars
    for path in tmpl_path.glob('*.groovy.j2'):
        path_str = str(path)

        # split off the actual template name which
        # is the last part of the path and generate
        # the pipeline
        pipeline_tmpl = path_str.split('/')[-1]
        pipeline_file = pipeline.generate(pipeline_tmpl)

        # get the groovy file name, i.e. drop the '.j2'
        pipeline_groovy_name = pipeline_tmpl[:-3]

        # place it as a groovy file under vars
        with open(vars_path / pipeline_groovy_name,
                  mode='w') as pipeline_library:
            pipeline_library.write(pipeline_file)


if __name__ == '__main__':
    generate_shared_lib()
