#!/usr/bin/env bash

docker run --rm --name go_env -td -v \
  /Users/apoehlmann/workspace/:/home/apoehlmann/workspace \
  apoehlmann/go-env sh