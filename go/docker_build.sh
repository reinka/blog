#!/usr/bin/env bash

docker build -t apoehlmann/go-env \
  --no-cache \
  --build-arg UID=$(id -u) --build-arg GID=$(id -g) \
  --build-arg HOME=/home/$(whoami) --build-arg USER=$(whoami) \
  .
