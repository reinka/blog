#!/usr/bin/env python3
import tensorflow as tf
import rospy

from std_msgs.msg import String

def talker():
    pub = rospy.Publisher('chatter', String, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10)  # 10hz
    with tf.Session() as sess:
        while not rospy.is_shutdown():
            hello_str = "hello world %s" % sess.run(
                tf.random.normal(shape=(1,)))  # sample from standard normal
            rospy.loginfo(hello_str)
            pub.publish(hello_str)
            rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
