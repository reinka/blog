#!/usr/bin/env bash
set -e

if [ ! -d /notebooks/workspace/src/tutorial ]; then
    cd /notebooks/workspace/src
    catkin_create_pkg tutorial
    ln -s /notebooks/workspace/src/scripts tutorial/
fi

exec "$@"
